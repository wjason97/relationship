<?php
if (!defined('IN_IA')) {
	exit('Access Denied');
}

return array(
	'version' => '1.0',
	'id'      => 'relationship',
	'name'    => '关系图',
	'v3'      => true,
	'menu'    => array(
		'plugincom' => 1,
		'icon'      => 'page',
		'items'     => array(
			array('title' => '关系图', 'route' => 'diagram'),
			array('title' => '环路关系', 'route' => 'diagram.abnormal_agents'),
		)
	)
);
