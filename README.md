## 人人商城 人人分销树状关系插件

## 声明
请购买官方正版人人商城后安装此插件，本源码只是基于官方程序开发的一款外围插件。
本插件源码永久免费开源。不限制用户商业用途。

## 依赖
本插件依赖vuejs 

## 安装方法
 - 在ewei_shopv2/plugin/ 目录下克隆当前项目
 - 导入relationship.sql 
 - 在商城插件管理中勾选开启插件【分销关系树状图】

## 效果如下
![效果](doc/img/introduce.png)
![效果](doc/img/abnormal_agents.png)