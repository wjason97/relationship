<?php if (!defined("IN_IA")) {
    exit("Access Denied");
}
class diagram_EweiShopV2Page extends PluginWebPage
{
    public function __construct()
    {
        global $_W;
        global $_GPC;
        $commissionLevel = p('commission')->getLevels();
        foreach ($commissionLevel as $lev) {
            $pu_Levels[$lev['id']] = $lev['levelname'];
        }
        $this->commissionLevel = $pu_Levels;
    }
    // 正常
    public function main()
    {
        global $_W, $_GPC;

        $agentid = $_GPC['agentid'];

        if (empty($agentid)) {
            $url =  'http://' . $_SERVER['HTTP_HOST'] . '/web/index.php?c=site&a=entry&m=ewei_shopv2&do=web&r=relationship.diagram.diagram_tree';
        } else {
            $url = 'http://' . $_SERVER['HTTP_HOST'] . '/web/index.php?c=site&a=entry&m=ewei_shopv2&do=web&r=relationship.diagram.diagram_tree&agentid=' . $agentid;
        }
        include $this->template();
    }
    public function diagram_tree()
    {
        global $_W;
        global $_GPC;
        
        //查询所有上级不是总店的分销商
        $effective_commissions = pdo_fetchall('select m.id,m.openid,m.agentid,m.agentlevel,m.isagent,m.nickname,m.realname,m.avatar,m.mobile,am.nickname as amnickname from ' . tablename('ewei_shop_member') . ' as m left join ' . tablename('ewei_shop_member') . ' as am on am.id=m.agentid  where m.uniacid=' . $_W['uniacid'] . ' ');
        $agentid = intval($_GPC['agentid']) ? intval($_GPC['agentid']) : 0;
        //如果没有参数则顶端为总店
        if (empty($agentid)) {
            $list['nickname'] = '总店';
            $list['realname'] = '总店';
            $list[''] = false;
            $list['avatar'] = "../addons/ewei_shopv2/static/images/noface.png";
            $list['id'] = 0;
        } else {
            foreach ($effective_commissions as $value) {
                if ($value['id'] == $agentid) {
                    if (empty($value['agentid'])) {
                        $value['amnickname'] = '总店';
                    }
                    $list = $value;
                }
            }
            unset($value);
            if (!empty($list) && !empty($list['agentlevel']) && !empty($this->commissionLevel)) {
                $list['level_name'] = $this->commissionLevel[$list['agentlevel']];
            }
        }
        foreach ($effective_commissions as $value) {
            if ($value['agentid'] == $agentid) {
                $list['children'][] = $value;
            }
        }
        unset($value);

        if (!empty($list['children'])) {
            foreach ($list['children'] as &$us) {
                if (!empty($us) && !empty($us['agentlevel']) && !empty($this->commissionLevel)) {
                    $us['level_name'] = $this->commissionLevel[$us['agentlevel']];
                }
                $childeren = $this->get_down_tree($us['id'], $path = 0, $effective_commissions);
                if (!empty($childeren)) {
                    $us['children'] = $childeren;
                }
            }
        }
        load()->func("tpl");

        include($this->template());
    }

    //递归查询下级
    public function get_down_tree($agentid, $path, &$effective_commissions)
    {
        global $_W;
        global $_GPC;
        $effectives=[];
        foreach ($effective_commissions as $key => $value) {

            if ($value['agentid'] == $agentid) {
                if (!empty($value) && !empty($value['agentlevel']) && !empty($this->commissionLevel)) {
                    $value['level_name'] = $this->commissionLevel[$value['agentlevel']];
                }
                $effectives[] = $value;
                unset($effective_commissions[$key]);
            }
        }

        unset($value, $key);
        if ($path > 1000) { //递归次数溢出 最多显示20级  实际数减3就对了
            return;
        }
        $path++;
        $childeren = array();
        if (!empty($effectives)) {
            foreach ($effectives as $key => $value) {

                $childeren = $this->get_down_tree($value['id'], $path = 0, $effective_commissions);
                if (!empty($childeren)) {
                    $effectives[$key]['children'] = $childeren;
                }
            }
        }

        return  $effectives;
    }

    //非正常会员
    public function abnormal_agents()
    {
        global $_W;
        global $_GPC;

        //查询所有会员的上级  一直往上查 如果查到某一个上级的agentid是自己的那么就认定为非正常上级
        $agentid = intval($_GPC['agentid']) ? intval($_GPC['agentid']) : 0;
        //如果没有参数则顶端不为总店
        if (empty($agentid)) {
            $list = pdo_getall('ewei_shop_member', array('agentid !=' => 0, 'status' => 1, 'uniacid' => $_W['uniacid']), array('nickname', 'avatar', 'agentlevel', 'agentid', 'id'));
        } else {
            $list[] = pdo_fetch('select id,agentid,nickname,avatar,agentlevel from ' . tablename('ewei_shop_member') . ' where uniacid=' . $_W['uniacid'] . ' and =1 and id=' . $agentid . ' ');
        }
        // var_dump($list);die;
        foreach ($list as $key => $value) {
            $agents[] = $value;
            // if($key>500){
            //     continue;
            // }

            if (!empty($value) && !empty($value['agentlevel']) && !empty($this->commissionLevel)) {
                $value['rank_name'] = $this->commissionLevel[$value['agentlevel']];
            }

            $is_abnormal = $this->get_abnormal_subordinate($value, $value, $agents);

            if ($is_abnormal) {
                $agents[] = $value;
                $abnormal_arr[] = $agents;
            }
            unset($agents);
        }
        // var_dump($abnormal_arr);
        load()->func("tpl");
        include($this->template());
    }
    //递归查询并验证是否非正常上级
    public  function get_abnormal_subordinate($old_us, $user, &$agents)
    {
        global $_W;
        global $_GPC;
        // 如果自己是自己上级
        if ($user['agentid'] == $old_us['id']) {
            $agents[] = $user;
            return 1;
        }

        $user = pdo_fetch('select id,agentid,nickname,avatar from ' . tablename('ewei_shop_member') . ' where uniacid=' . $_W['uniacid'] . ' and id=' . $user['agentid'] . ' ');

        $i = 0;
        while ($user) {
            $i++;
            if (!empty($user)) {
                $agents[] = $user;

                if ($old_us['id'] == $user['agentid']) {
                    return 1;
                } else {
                    if (empty($user['agentid'])) {
                        return 0;
                    }
                    $user = pdo_fetch('select id,agentid,nickname,avatar from ' . tablename('ewei_shop_member') . ' where uniacid=' . $_W['uniacid'] . ' and id=' . $user['agentid'] . ' ');
                    return $this->get_abnormal_subordinate($old_us, $user, $agents);
                }
            } else {
                return 0;
            }
        }
        return 0;
    }
}
