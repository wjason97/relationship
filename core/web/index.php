<?php

if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Index_EweiShopV2Page extends PluginWebPage {

    function main() {
        global $_W;
        if (cv('relationship.agent')) {
            
            header('location: ' . webUrl('relationship/diagram'));
            exit;
        } 
    }


}
